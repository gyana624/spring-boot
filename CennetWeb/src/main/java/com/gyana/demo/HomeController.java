package com.gyana.demo;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;


@Controller
public class HomeController 
{
	@RequestMapping("home")
	
	public ModelAndView home(Student stu)
	{	
		ModelAndView mv=new ModelAndView();
		mv.addObject("obj",stu);
		mv.setViewName("home");
		
		return mv;
	}

}
