package com.gyana.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CennetWebApplication {

	public static void main(String[] args) {
		SpringApplication.run(CennetWebApplication.class, args);
	}

}
