package com.gyana.FirstCennet;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

import ch.qos.logback.core.net.SyslogOutputStream;

@SpringBootApplication
public class FirstCennetApplication {

	public static void main(String[] args) {
		ConfigurableApplicationContext context = SpringApplication.run(FirstCennetApplication.class, args);
	
		Cennetpro a=context.getBean(Cennetpro.class);
		a.show();
		
	}

}
